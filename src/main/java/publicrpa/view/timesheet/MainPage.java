package publicrpa.view.timesheet;

import org.openqa.selenium.By;
import org.openqa.selenium.HasAuthentication;
import org.openqa.selenium.UsernameAndPassword;

import publicrpa.view.AbstractBrowserView;

public class MainPage extends AbstractBrowserView {

	public MainPage loadAndLogin(String login, String password) {

        ((HasAuthentication) driver).register(UsernameAndPassword.of(login, password));
        driver.get("https://ts.euvic.pl/");
        takeScreenshotFromFlow();

        try {
        	waitForVisibilityOfElementLocated(By.id("userNameInput")).sendKeys(login);
			driver.findElement(By.id("passwordInput")).sendKeys(password);
			driver.findElement(By.id("submitButton")).click();
		} catch (Exception nonimportant) {}

        waitForVisibilityOfElementLocated(By.linkText("Projects"), 90);
        return this;
    }

	public EntriesReportFormPage goToReportFormPage() {
        driver.findElement(By.linkText("Entries in Timesheet")).click();
		return new EntriesReportFormPage();
	}

}
