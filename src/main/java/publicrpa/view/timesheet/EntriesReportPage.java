package publicrpa.view.timesheet;

import org.openqa.selenium.By;

import publicrpa.view.AbstractBrowserView;

public class EntriesReportPage extends AbstractBrowserView {

	public EntriesReportPage downloadXlsxFile() {
        driver
        	.findElements(By.tagName("span"))
			.stream()
			.filter(e -> e.getText().trim().toLowerCase().equals("export visible columns"))
			.findAny()
			.get()
			.click();
        return this;
	}
}
