package publicrpa.view.timesheet;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.interactions.Actions;

import publicrpa.view.AbstractBrowserView;

public class EntriesReportFormPage extends AbstractBrowserView {

	public EntriesReportPage generateReportForAllInLastFourWeeks() {
        selectAllEntries();

        final var lastNWeeksUp = driver.findElement(By.id("lastNWeeks")).findElement(By.xpath("./..")).findElement(By.tagName("span")).findElement(By.tagName("span"));
        lastNWeeksUp.click();
        lastNWeeksUp.click();

        submitSelection();

        return new EntriesReportPage();
	}

	private void submitSelection() {
		driver.findElements(By.tagName("button"))
        		.stream()
        		.filter(e -> e.getText().trim().toLowerCase().equals("open"))
        		.findAny()
        		.get()
        		.click();
        mitigateStaleElementException();
        waitForLowerCaseTextInOneOfmanyElementsLocated(By.tagName("span"), "clear filters and sorting");
	}

	private void selectAllEntries() {
		final var allEntriesCheckbox = waitForPresenceOfElementLocated(By.className("checkmark"));
        new Actions(driver).moveToElement(allEntriesCheckbox).build().perform();
        allEntriesCheckbox.click();
	}

	private void mitigateStaleElementException() {
		try {
            waitForLowerCaseTextInOneOfmanyElementsLocated(By.tagName("span"), "clear filters and sorting");
		} catch (StaleElementReferenceException e2) {}
        try {
            waitForLowerCaseTextInOneOfmanyElementsLocated(By.tagName("span"), "clear filters and sorting");
		} catch (StaleElementReferenceException e2) {}
	}

	public EntriesReportPage generateReportForAllInLastMonth() {
		selectAllEntries();
		clickLastMonthSelector();
		submitSelection();
		return new EntriesReportPage();
	}

	public EntriesReportPage generateReportForAllInLastNMonths(int months) {
        selectAllEntries();
        clickLastNMonthsSelector();
        final var lastNMonthsUp = driver.findElement(By.id("lastNMonths")).findElement(By.xpath("./..")).findElement(By.tagName("span")).findElement(By.tagName("span"));
        for (int i = 0; i < months; i++) {
        	lastNMonthsUp.click();
		}
        submitSelection();

        return new EntriesReportPage();
	}

	private void clickLastMonthSelector() {
		driver
			.findElements(By.tagName("span"))
			.stream()
			.filter(e -> e.getText().trim().toLowerCase().equals("last month"))
			.findAny()
			.get()
			.click();
	}

	private void clickLastNMonthsSelector() {
		driver
			.findElements(By.tagName("span"))
			.stream()
			.filter(e -> e.getText().trim().toLowerCase().equals("last months"))
			.findAny()
			.get()
			.click();
	}

}
